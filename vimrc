set nocompatible

syntax on

set shell=bash

let mapleader=","

set showmode

set tabstop=4

set backspace=indent,eol,start

set autoindent

set copyindent

"set number

set showmatch 

set ignorecase


set smartcase

set incsearch

set mouse=v

set fileformats="unix,mac,dos"

set shortmess+=I 

set clipboard=unnamed

set autoread

set updatetime=1000

set undolevels=1000

if v:version >= 730
    set undofile        
    set undodir=~/.vim/.undo,~/tmp
endif

set nomodeline


set ttyfast

if (has("termguicolors"))
   set termguicolors
endif
